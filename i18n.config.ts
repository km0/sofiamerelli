export default defineI18nConfig(() => ({
	legacy: false,
	locale: "it",
	messages: {
		en: {
			works: "Works",
			about: "About",
			films: "Films",
			other: "Other",
			description:
				"Italian filmmaker whose research is close to cinéma du réel, with a particular focus on post-natural studies.",
		},
		it: {
			works: "Lavori",
			about: "Bio",
			films: "Film",
			other: "Altro",
			description:
				"Filmmaker italiana la cui ricerca è prossima al cinema del reale, con una particolare attenzione verso i post natural studies.",
		},
	},
}));
