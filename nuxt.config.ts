// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	css: ["assets/css/font.css", "assets/css/style.css"],
	devtools: { enabled: false },
	runtimeConfig: {
		public: {
			frontend: "localhost:3000",
		},
	},
	modules: ["@vueuse/nuxt", "@nuxt/content", "@nuxtjs/i18n"],
	content: {
		defaultLocale: "it",
		locales: ["it", "en"],
	},
	i18n: {
		vueI18n: "./i18n.config.ts",
		locales: [
			{
				code: "it",
				name: "Italiano",
			},
			{
				code: "en",
				name: "English",
			},
		],
		defaultLocale: "it",
	},
	postcss: {
		plugins: {
			"postcss-nested": {},
		},
	},
});
