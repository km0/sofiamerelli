

const scale = ref(0.5)
const source = ref('/video/test.mp4')
const zoom = ref(0.999)
const opacity = ref(1)
const active = ref(true)



export const usePlayer = () => {

    const setScale = (s) => scale.value = s
    const setSource = (s) => source.value = s
    const setZoom = (z) => zoom.value = z
    const setOpacity = (o) => opacity.value = o


    const enablePlayer = () => {
        active.value = true
    }
    const disablePlayer = () => {
        active.value = false
    }

    return {
        scale: readonly(scale),
        zoom: readonly(zoom),
        source: readonly(source),
        opacity: readonly(opacity),
        active: readonly(active),
        setScale,
        setSource,
        setZoom,
        setOpacity,
        enablePlayer,
        disablePlayer
    }
}