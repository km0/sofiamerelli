---
title: "/web dji safoul : hello world"
slug: "dji-safoul"
year: 2019
other: true
---

_/web dji safoul : hello world_ is an investigation into the possible local manifestations of a global tool such as the Internet.
The project, developed together with the collective UN\*SALTA, originates from a mural in the high school of Koubanao (Casamance, Senegal) which reads: _Internet: to learn, to discover, to be closer. The world: a planetary village_.
The research is developed in workshop form together with the school students. From the exchange with them and professors, an experience of the Internet emerges that is strongly circumscribed to the large digital platforms, which, by offering limited free access to the Internet, make up for the country's infrastructural shortcomings, effectively establishing a monopoly.

The experimentation path for a local Internet initially takes the form of a crowdsourced French-Djola dictionary, programming a Telegram bot connected to a group chat that includes students, professors and us.
The final return of the workshops takes the form of a real-time translation of a text composed with the students; a collective performance at the foot of the village aqueduct and accessible online from all over the world via a website.

![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270627/sofiamerelli.com/dji-safoul/Dji_safoul1_tjnuuv.jpg)
![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270682/sofiamerelli.com/dji-safoul/Dji_safoul2_imbxyi.jpg)
![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270683/sofiamerelli.com/dji-safoul/Dji_safoul3_kuow76.jpg)
![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270628/sofiamerelli.com/dji-safoul/Dji_safoul4_tbsnlh.jpg)