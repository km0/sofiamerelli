---
title: Frana Futura
slug: "frana-futura"
cover: https://res.cloudinary.com/djma2xwfx/image/upload/v1705014533/sofiamerelli.com/Frana-Futura/Cave5_cmvwod.webp
year: WIP 2024
---

Liguria is an impervious region, constantly threatened by hydrogeological instability.

The Landslide has always drenched this environment: it is its geological (and ontological) conformation. Those who live here laboriously resist the sliding of the earth, a task that takes on the contours of care but also of forcing balances: terrace after terrace, stone after stone, the landscape becomes an immense deposit of fatigue.

Here, the sense of landslide goes beyond the common meaning: it is synonymous with neglect and abandonment, depopulation of inland areas and loss of local memory. But it is also a symbol of transformation and renewal, of material shuffling towards new geographies. A perpetual transformation that operates on temporal scales forbidden to us, like those of lithogenesis.

A device for breaking out of the anthropocentric perspective and attuning with the rhythms of the earth. In this humus of energies, it is the stone that configures itself as an interface between us and the landslide.

[franafutura.com](https://franafutura.com){target="\_blank"}

## Credits

-   _Screenwriting_: Sofia Merelli, Francesco Luzzana, Elena Bongiorno
-   _Direction_: Sofia Merelli
-   _Cinematography_: Sofia Merelli
-   _Drone Operator_: Francesco Luzzana
-   _Edit_: Sofia Merelli, Francesco Luzzana, Elena Bongiorno
-   _Sound design_: Francesco Luzzana
-   _Original Score_: Michael Papadopoulos
-   _Color_: Leonardo Masoero
