---
title: Future Comes at the Right Time
slug: "future-comes-at-the-right-time"
running_time: 25:56
format: Digital
year: 2022
vimeo_id: 902771057
---

As of today, sexual freedom and gender self-determination are punished with detention or death in several countries. Future comes at the right time tells the story of Nelson and his escape to Italy after a violent aggression. He was born 25 years ago in Benin City, one of the most unstable areas of Nigeria.
In 2018 the territorial commission that examined his case considered his testimony not sufficiently credible and rejected his request for international protection. In 2021 Nelson appealed to the Venice Court.
Awaiting for his verdict, which can take months, Nelson spends his days working, hanging out with friends and making music, hoping to become a great singer one day.

## Credits

_Subject_: Elena Bongiorno  
_Direction / Cinematography / Edit_: Elena Bongiorno, Sofia Merelli, Martina Tamburini, Gabriele Umidon  
_Original music_: Castro G, Michael Papadopoulos  
_Color grading_: Mattia Pintonello  
_Graphics_: Francesco Luzzana

## Screenings

_Beyond Borders_, Kastellorizo International Film Festival, Greece 2023  
_Florence Queer Festival_, Italia 2023  
_Festival del Cinema dei Diritti Umani_, Naples, Italy 2023  
_International Queer Film Festival_, Playa del Carmen, Mexico 2023  
_LGBT+ Film Festival_, Warsaw, Poland 2024

![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012798/sofiamerelli.com/Future-comes/Future_comes1_mhw5vm.jpg)
![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012803/sofiamerelli.com/Future-comes/Future_comes2_o4bbhk.jpg)
![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012809/sofiamerelli.com/Future-comes/Future_comes3_vlv5yl.jpg)
![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012816/sofiamerelli.com/Future-comes/Future_comes4_fiozy2.jpg)
