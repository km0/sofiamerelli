---
title: Nel girare l’angolo
full_title: |-
    Nel girare l'angolo
    raccogliere un filo
    sulla via del ritorno
year: 2018
running_time: 10:23
format: Single channel, HD
thumb: /video/angolo.mp4
watch: /video/angolo.mp4
slug: "allangolare"
---

My grandfather got on his bike at 16 and rode down from the Seriana Valley to Milan.
He was looking for employment in the big city. He took a wife and a house, but died young of lung cancer.
And so, at the age of 16, my father also started working to support the family.

In this project I retrace my grandfather's route back to his mountaintop birthplace. A place where I had never been, but where I belonged.
The images of the journey are interwoven with those of my childhood films, another place from the past where I come from and which I have partly forgotten.

![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836569/sofiamerelli.com/nel-girare-l-angolo/Nel_girare1.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836568/sofiamerelli.com/nel-girare-l-angolo/Nel_girare2.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836569/sofiamerelli.com/nel-girare-l-angolo/Nel_girare3.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836569/sofiamerelli.com/nel-girare-l-angolo/Nel_girare4.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836568/sofiamerelli.com/nel-girare-l-angolo/Nel_girare5.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836570/sofiamerelli.com/nel-girare-l-angolo/Nel_girare6.webp)
