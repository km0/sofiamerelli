---
title: Hummmus
slug: hummmus
running_time: 2:24
format: Digital
year: 2019
vimeo_id: 903480940
---

A deserted cloistered monastery is emptied with the help of asylum seekers from the S. Vincenzo patronage in Bergamo.

![Hummus](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270753/sofiamerelli.com/Hummmus/Hummmus1_ehkshh_huepbv.jpg)
![Hummus](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270751/sofiamerelli.com/Hummmus/Hummmus3_cu5aqh_mna0su.jpg)
![Hummus](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270752/sofiamerelli.com/Hummmus/Hummmus4_i4dais_mvfgay.jpg)
