---
title: Stato di Grazia
year: 2018
running_time: 5:42
format: Digital
slug: "stato-di-grazia"
---

A girl wanders through an inhospitable, rocky landscape.  
Everyday gestures like a prayer striving towards a state of grace.  
An especially happy summer.

![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838341/sofiamerelli.com/stato-di-grazia/Stato_di_grazia1_etdb2l.jpg)
![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838344/sofiamerelli.com/stato-di-grazia/Stato_di_grazia2_vkg7jz.jpg)
![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838348/sofiamerelli.com/stato-di-grazia/Stato_di_grazia3_p8b2mm.jpg)
![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838352/sofiamerelli.com/stato-di-grazia/Stato_di_grazia4_xosptl.jpg)
