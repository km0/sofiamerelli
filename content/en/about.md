## Bio

Sofia Merelli is an Italian filmmaker whose research is close to _cinéma du réel_, with a particular focus on post-natural studies. Her path crosses different artistic practices and she was part of the UN\*SALTA collective, with which she realised two interactive performances. However, the language that most represents her is audiovisual and she is currently editing her first medium-length film Frana Futura.

## Education

-   Documentary filmmaking, Civica Scuola di Cinema Luchino Visconti, Milan 2020 - 2022
-   BA Art and media, Accademia di Belle Arti di Brera, Milan, 2016 - 2020
-   &nbsp;
-   _L’Orto del Mondo - Cinéma du réel workshop about vegetal realities_ with Giovanni Cioni, 2023
-   _Other Realities - Documentary and fiction: on the edge of an eternal creative challenge_ - Edited by Ludovica Fales, 2022
-   _Other Spaces #2 - Documentary and Urban Space_ - Workshop with Michelangelo Frammartino, 2022
-   _Postnature and Contemporary Creation_, seminar at Institute for Postnatural Studies, 2022
-   _Cinema with Hands_ with Giuseppe Baresi and Unzalab, 2019
-   _Found Footage_ with Alina Marazzi and Unzalab, ​2019

## Screenings

-   _Future Comes at the Right Time_, LGBT+ Film Festival, Warsaw, Poland 2024
-   _Future Comes at the Right Time_, International Queer Film Festival, Playa del Carmen, Mexico 2023
-   _Future Comes at the Right Time_, Festival del Cinema dei Diritti Umani, Naples, Italy 2023
-   _Future Comes at the Right Time_, Florence Queer Festival, Italia 2023
-   _Future Comes at the Right Time_, Beyond Borders, Kastellorizo International Film Festival, Greece 2023
-   _T.A.RI._, The Wrong Biennale, Rifting Pavillon, 2021

## Performances

-   _/web dji safoul : hello world_ - Reportage for a local internet in Senegal,
    UN\*SALTA collective, Koubanao, Senegal, 2019
-   _La passeggiata_, UN\*SALTA collective, Utopia Fest, Agrigento, Italia 2019

## Contatti

[sofiamerelli@gmail.com](mailto:sofiamerelli@gmail.com)
