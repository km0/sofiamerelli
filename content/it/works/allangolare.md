---
title: Nel girare l’angolo
full_title: |-
    Nel girare l'angolo
    raccogliere un filo
    sulla via del ritorno
year: 2018
running_time: 10:23
format: Digitale
thumb: /video/angolo.mp4
watch: /video/angolo.mp4
slug: "allangolare"
---

Mio nonno a 16 anni montò in sella alla sua bici e dalla Val Seriana scese a Milano.
Cercava un impiego nella grande città. Prese moglie e prese casa, ma morì giovane per un cancro ai polmoni.
E così, anche mio padre a 16 anni iniziò a lavorare per mantenere la famiglia.

In questo progetto ripercorro a ritroso il percorso fatto da mio nonno, fino al suo paese natale in cima a monti. Un luogo dove non ero mai stata, ma a cui appartengo.
Alle immagini del viaggio si intrecciano quelle dei filmini della mia infanzia, un altro luogo del passato da cui provengo e in parte dimenticato.

![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836569/sofiamerelli.com/nel-girare-l-angolo/Nel_girare1.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836568/sofiamerelli.com/nel-girare-l-angolo/Nel_girare2.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836569/sofiamerelli.com/nel-girare-l-angolo/Nel_girare3.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836569/sofiamerelli.com/nel-girare-l-angolo/Nel_girare4.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836568/sofiamerelli.com/nel-girare-l-angolo/Nel_girare5.webp)
![Nel girare l'angolo](https://res.cloudinary.com/djma2xwfx/image/upload/v1704836570/sofiamerelli.com/nel-girare-l-angolo/Nel_girare6.webp)
