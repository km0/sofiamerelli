---
title: Frana Futura
slug: "frana-futura"
cover: https://res.cloudinary.com/djma2xwfx/image/upload/v1705014533/sofiamerelli.com/Frana-Futura/Cave5_cmvwod.webp
year: WIP 2024
---

La Liguria è una regione impervia e costantemente minacciata da dissesto idrogeologico.

La Frana impregna da sempre questo territorio, è la sua conformazione geologica (e ontologica) naturale. Chi vi abita si oppone faticosamente allo scivolare via del suolo, un lavoro che prende la forma di cura ma anche di forzatura degli equilibri. Terrazzamento dopo terrazzamento, pietra dopo pietra, il paesaggio diventa un immenso deposito di fatiche.

Qui, il significato di Frana sconfina oltre l'accezione comune: è sinonimo di incuria e abbandono, spopolamento delle aree interne e perdita della memoria locale. Ma è anche un simbolo di trasformazione e rinnovamento, di materiale che si rimescola verso nuove geografie. Una trasformazione perpetua che opera su scale temporali a noi interdette, come quelle della litogenesi.

Un dispositivo per allontanarsi dalla prospettiva antropocentrica e risintonizzarsi con i ritmi della terra. In questo humus di energie, è la pietra a configurarsi come elemento di interfaccia tra noi e la Frana.

[franafutura.com](https://franafutura.com){target="\_blank"}

## Credits

-   _Scrittura_: Sofia Merelli, Francesco Luzzana, Elena Bongiorno
-   _Regia_: Sofia Merelli
-   _Fotografia_: Sofia Merelli
-   _Operatore Drone_: Francesco Luzzana
-   _Montaggio_: Sofia Merelli, Francesco Luzzana, Elena Bongiorno
-   _Sound design_: Francesco Luzzana
-   _Musiche originali_: Michael Papadopoulos
