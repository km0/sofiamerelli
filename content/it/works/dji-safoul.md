---
title: "/web dji safoul : hello world"
slug: "dji-safoul"
year: 2019
other: true
---

_/web dji safoul : hello world_ è un’indagine sulle possibili manifestazioni locali di uno strumento globale come internet.
Il progetto, sviluppato assieme al collettivo UN\*SALTA, prende origine da un murale nel liceo di Koubanao (Casamance, Senegal) che recita: _Internet: to learn, to discover, to be closer. The world: a planetary village_.
La ricerca viene sviluppata in forma laboratoriale assieme agli alunni della scuola. Dallo scambio con gli studenti e i professori emerge un’esperienza della rete fortemente circoscritta alle grandi piattaforme digitali, che offrendo gratuitamente un limitato accesso a internet sopperiscono alle mancanze infrastrutturali del Paese, di fatto istituendo un monopolio.

Il percorso di sperimentazione per un internet locale prende inizialmente la forma di un dizionario francese-djola crowdsourced, programmando un bot Telegram collegato a una chat di gruppo che include studenti, professori e noi.
La restituzione finale dei workshop si configura come una traduzione in real time di un testo composto a più mani con i ragazzi; una performance collettiva ai piedi dell’acquedotto del villaggio e accessibile online da tutto il mondo tramite un sito.

![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270627/sofiamerelli.com/dji-safoul/Dji_safoul1_tjnuuv.jpg)
![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270682/sofiamerelli.com/dji-safoul/Dji_safoul2_imbxyi.jpg)
![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270683/sofiamerelli.com/dji-safoul/Dji_safoul3_kuow76.jpg)
![Dji Safoul](https://res.cloudinary.com/djma2xwfx/image/upload/v1705270628/sofiamerelli.com/dji-safoul/Dji_safoul4_tbsnlh.jpg)|