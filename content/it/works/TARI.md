---
title: T.A.RI.
slug: tari
year: 2019
running_time: 4:40
format: 3D animation
vimeo_id: 632409885
---

Tanto Andrò a RIcomprarlo è il punto di vista di una lavatrice riguardo le politiche usa e getta della produzione capitalista.

## Screening

_The Wrong Biennale_, Rifting Pavillon, 2021

![TARI 1](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012201/sofiamerelli.com/Tari/Tari1_ymrb51.jpg)
![TARI 2](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012199/sofiamerelli.com/Tari/Tari2_zg9ofk.jpg)
![TARI 3](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012201/sofiamerelli.com/Tari/Tari3_z0iahd.jpg)
![TARI 4](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012200/sofiamerelli.com/Tari/Tari4_bzg2cp.jpg)
