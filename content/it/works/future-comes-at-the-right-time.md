---
title: Future Comes at the Right Time
slug: "future-comes-at-the-right-time"
year: 2022
vimeo_id: 902771057
---

Sono ancora molti i paesi nel mondo in cui la libera sessualità e l’autodeterminazione di genere sono punite con la detenzione o la morte. Future comes at the right time racconta la storia di Nelson, nato 25 anni fa a Benin City, una delle province più instabili della Nigeria, e della sua fuga in Italia a seguito di una violenta aggressione. Nel 2018, la commissione territoriale che ha esaminato il suo caso ha reputato la sua testimonianza non credibile, rifiutando così la domanda di protezione internazionale. Nel 2021 Nelson ha ricorso in appello presso il Tribunale di Venezia. In attesa del verdetto, che può richiedere diversi mesi, Nelson si divide tra il lavoro, le amicizie e la musica, con la speranza di diventare un giorno un grande cantante.

## Credits

_Soggetto_: Elena Bongiorno  
_Regia, Fotografia, Montaggio_: Elena Bongiorno, Sofia Merelli, Martina Tamburini, Gabriele Umidon  
_Musiche_: Castro G, Michael Papadopoulos  
_Color_: Mattia Pintonello  
_Grafiche_: Francesco Luzzana

## Screenings

_Beyond Borders_, Kastellorizo International Film Festival, Grecia 2023  
_Florence Queer Festival_, Italia 2023  
_Festival del Cinema dei Diritti Umani_, Napoli, Italia 2023  
_International Queer Film Festival_, Playa del Carmen, Messico 2023  
_LGBT+ Film Festival_, Varsavia, Polonia 2024

![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012798/sofiamerelli.com/Future-comes/Future_comes1_mhw5vm.jpg)
![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012803/sofiamerelli.com/Future-comes/Future_comes2_o4bbhk.jpg)
![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012809/sofiamerelli.com/Future-comes/Future_comes3_vlv5yl.jpg)
![Future Comes at the Right Time](https://res.cloudinary.com/djma2xwfx/image/upload/v1705012816/sofiamerelli.com/Future-comes/Future_comes4_fiozy2.jpg)
