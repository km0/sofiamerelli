---
title: Stato di Grazia
year: 2018
running_time: 5:42
format: Digitale
slug: "stato-di-grazia"
---

Una ragazza vaga in un paesaggio inospitale e roccioso.  
I gesti del quotidiano come preghiera tesa verso uno stato di grazia.  
Un’estate particolarmente felice.

![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838341/sofiamerelli.com/stato-di-grazia/Stato_di_grazia1_etdb2l.jpg)
![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838344/sofiamerelli.com/stato-di-grazia/Stato_di_grazia2_vkg7jz.jpg)
![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838348/sofiamerelli.com/stato-di-grazia/Stato_di_grazia3_p8b2mm.jpg)
![Stato di grazia](https://res.cloudinary.com/djma2xwfx/image/upload/v1704838352/sofiamerelli.com/stato-di-grazia/Stato_di_grazia4_xosptl.jpg)
