## Bio

Sofia Merelli è una filmmaker italiana la cui ricerca è prossima al cinema del reale, con una particolare attenzione verso i post natural studies. Il suo percorso attraversa diverse pratiche artistiche ed è stata parte del collettivo UN\*SALTA, con il quale ha realizzato due performance interattive. Il linguaggio che più la rappresenta è tuttavia quello audiovisivo ed è attualmente in fase di montaggio del suo primo mediometraggio Frana Futura.

## Istruzione

-   Corso di Documentario, Civica Scuola di Cinema Luchino Visconti, Milano, 2020 - 2022
-   Nuove Tecnologie dell’Arte, Accademia di Belle Arti di Brera, Milano, 2016 - 2020
-   &nbsp;
-   _L’Orto del Mondo - Laboratorio di cinema del reale sul reale vegetale_ con Giovanni Cioni, 2023
-   _Other Realities - Documentario e finzione: ai confini di un’eterna sfida creativa_ - A cura di Ludovica Fales, 2022
-   _Other Spaces #2 - Documentario e spazio urbano_ – Workshop con Michelangelo Frammartino, 2022
-   _Institute for Postnatural Studies_, Seminario Postnature and Contemporary Creation, 2022
-   _Found Footage_ con Alina Marazzi e Unzalab, ​2019
-   _Il Cinema con le Mani_ con Giuseppe Baresi e Unzalab, 2019

## Screenings

-   _Future Comes at the Right Time_, LGBT+ Film Festival, Varsavia, Polonia 2024
-   _Future Comes at the Right Time_, International Queer Film Festival, Playa del Carmen, Messico 2023
-   _Future Comes at the Right Time_, Festival del Cinema dei Diritti Umani, Napoli, Italia 2023
-   _Future Comes at the Right Time_, Florence Queer Festival, Italia 2023
-   _Future Comes at the Right Time_, Beyond Borders, Kastellorizo International Film Festival, Grecia 2023
-   _T.A.RI._, The Wrong Biennale, Rifting Pavillon, 2021

## Performance

-   _/web dji safoul : hello world_ - Reportage per un internet locale in Senegal, collettivo UN\*SALTA, Koubanao, Senegal, 2019
-   _La passeggiata_, collettivo UN\*SALTA, Utopia Fest, Agrigento, Italia 2019

## Contatti

[sofiemerelli@gmail.com](mailto:sofiamerelli@gmail.com)
